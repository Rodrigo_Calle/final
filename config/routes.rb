#Rails.application.routes.draw do
  #get 'sessions/new'
  #get 'sessions/create'
 # get 'sessions/destroy'
  #resources :users
  #get 'home/index'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
 # root "home#index"

Rails.application.routes.draw do
  root 'home#cara'

  get "home/index"
  
#<get '*path' => redirect('/')>  
  get "sessions/perfil"
  resources :users

  resources :sessions, only: [ :new, :create, :destroy]
  
  get "signup", to: "users#new", as: "signup"
  get "login" ,to: 'sessions#new', as: "login"
  get "logout", to: "sessions#destroy", as: "logout"
end
#end
